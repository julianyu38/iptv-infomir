<?php 
	chdir(dirname(__FILE__));

	//se consultan los dispositivos 

	date_default_timezone_set("America/Mexico_City");

	include "../conexion.php";

	echo "PROTVPLUS Inicia verificacion de la vigencia de los dispositivos";

	echo "\n\n";

	$sql = "SELECT 
				td.id_cliente,
				td.id_dispositivo, 
				datediff( fecha_vigencia, now() ) as diferencia,
				fecha_vigencia,
				nombre_cliente
			FROM 
				t_dispositivos as td
					inner join 
				t_clientes as tc on (td.id_cliente = tc.id_cliente)
			ORDER BY 
				td.id_cliente, fecha_vigencia desc";

	$res2 = mysqli_query($conexion,$sql);

	while($ln = mysqli_fetch_array($res2) ){

		if($ln['diferencia'] < 1){ //es un dispositivo con fecha de vigencia el dia de mañana

			echo "Cliente: ".$ln['nombre_cliente']."\nDispositivo proximo a vencer: ".$ln['id_dispositivo']."\nFecha de vencimineto: ".$ln['fecha_vigencia']."\n";
			//se consulta el saldo del cliente 
			$sql = "SELECT 
					    sum(monto) as saldo
					FROM
					    t_movimientos_clientes
					where	
						id_cliente = '$ln[id_cliente]'";

			$res3 = mysqli_query($conexion,$sql);
			$ln2 = mysqli_fetch_array($res3);
			$saldo = $ln2['saldo'];

			if($saldo < 1 ){
				//no hay saldo
				echo "No hay saldo";
			}else{

				$sql = "INSERT INTO `t_movimientos_clientes`
						(
						`id_cliente`,
						`monto`,
						`id_usuario_registro`
						)
						VALUES
						(
						'$ln[id_cliente]',
						'-1',
						'1'
						);";

				mysqli_query($conexion,$sql);

				$fecha_vigencia = Date('Y/m/d', strtotime("+32 days"));

				$sql = "UPDATE `t_dispositivos`
						SET
						`fecha_vigencia` = '$fecha_vigencia'
						WHERE 
							`id_dispositivo` = '$ln[id_dispositivo]';";

				mysqli_query($conexion,$sql);	
				echo "Vigencia renovada";
			}	

			echo "\n\n";
		}
	}

	echo "Finaliza verificacion de la vigencia de los dispositivos";
 ?>