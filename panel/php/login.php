<?php 
	//se inicia sesion
	session_start();

	/*print_r($_POST);
	die();*/
	
	//si no se resivio el usuario se regresa a la pagina anterior
	if(!isset($_POST['usuario'])){
		header("Location: ../index.php");
	}
	if(!isset($_POST['password'])){
		header("Location: ../index.php");
	}

	//se incluye la conexion a la base de datos
	include "conexion.php";

	
	//en caso de ser un login exitoso se almacena como cookie de ser solicitado
	if( isset($_POST['recordar_password']) ){
		//se almacena la cookie
		setcookie("usuario",   			$_POST['usuario'],   time()+60*60*24*30, "/");
		setcookie("password", 			$_POST['password'], time()+60*60*24*30, "/");
		setcookie("recordar_password",  "1",   				time()+60*60*24*30, "/");

	}else{
		//se borran las cookies
		setcookie('usuario', null, -1, "/");
		setcookie('password', null, -1, "/");
		setcookie('recordar_password', null, -1, "/");
	}


	//consulta a la base de datos 
	$sql = "SELECT 
			    id_usuario,
			    nombre,
			    tipo_usuario,
			    estatus
			FROM
			    t_usuarios
			WHERE
			    usuario = '$_POST[usuario]' AND
			    pass = '$_POST[password]';";
	
	$res = mysqli_query($conexion, $sql) or die(mysqli_error($conexion));

	//si solo se encunetra un resultado significa login exitoso
	if( mysqli_num_rows($res) == 1 ){ //si el resultado da un solo resultado
		
		$linea = mysqli_fetch_array($res, MYSQLI_ASSOC);
		
		$_SESSION["id_usuario"] 	= $linea["id_usuario"];
    	$_SESSION["nombre"]   		= $linea["nombre"];
    	$_SESSION["tipo_usuario"]   = $linea["tipo_usuario"];

    	if($linea["estatus"] == 'A'){

    		if($linea["tipo_usuario"] != 'V'){
    			header('Location: ../usuarios.php');
    		}else{
    			header('Location: ../clientes.php');
    		}
			
    	}else{
    		header('Location: ../index.php?i=1');
    	}
		

	}else{//si da mas resultados o ninguno la consulta retorna error de autentificacion
		header('Location: ../index.php?i=0');
	}
	
	mysqli_close($conexion);
 ?> 