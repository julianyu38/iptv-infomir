<?php 
	include "../conexion.php";

	session_start();

	if(!isset($_SESSION["id_usuario"])){
		header("location: index.php");
		die();
	}

	if($_SESSION["tipo_usuario"] == "V"){ //solo para administradores y super vendedores
		header("location: index.php");
		die();
	}

	//si el usuario no es administrador se resta primero el credito de su cuenta en operaciones aumentar

	if($_SESSION["tipo_usuario"] == "S"){ 

		if($_POST['operacion'] == "aumentar"){ //la operacion aumenta el saldo de un vendedor y reduce la del super vendedor

			//se consulta el saldo 
			$sql = "SELECT 
					    sum(monto) as saldo
					FROM
					    t_movimientos_usuarios
					where	
						id_usuario = '$_SESSION[id_usuario]'";

			$res = mysqli_query($conexion,$sql) or die($sql);
			$ln = mysqli_fetch_array($res);
			$saldo = $ln['saldo'];

			if($saldo < $_POST['monto'] ){
				//se detiene el proceso pues no se puede asignar saldo que no se tiene al vendedor
				$linea['resultado'] = "0"; //fecha y hora ocupada
            	$linea['mensaje'] = "You do not have enough balance to do the operation";
            	echo json_encode($linea);
            	die();
			}else{
				//se procede con el retiro del credito
				$monto = "-".$_POST['monto'];
			}

		}else{
			//la operacion disminuye el saldo del vendedor y aumenta la del supervendedor

			//se consulta el saldo 
			$sql = "SELECT 
					    sum(monto) as saldo
					FROM
					    t_movimientos_usuarios
					where	
						id_usuario = '$_POST[id_usuario]'";

			$res = mysqli_query($conexion,$sql);
			$ln = mysqli_fetch_array($res);
			$saldo = $ln['saldo'];

			if($saldo < $_POST['monto'] ){
				//se detiene el proceso pues no se puede restar el saldo que no se tiene al vendedor
				$linea['resultado'] = "0"; //fecha y hora ocupada
            	$linea['mensaje'] = "The target seller does not have enough balance to do the operation";
            	echo json_encode($linea);
            	die();
			}else{
				//se procede con el aumento del credito
				$monto = $_POST['monto'];
			}

		}

		$sql = "INSERT INTO `t_movimientos_usuarios`
				(
					`id_usuario`,
					`monto`,
					`id_usuario_registro`
				)
				VALUES
				(
					'$_SESSION[id_usuario]',
					'$monto',
					'$_SESSION[id_usuario]'
				);";

		$res = mysqli_query($conexion,$sql);

		if(!$res){
			
	        $linea['resultado'] = "0";
	        $linea['mensaje'] = mysqli_error($conexion);

	        echo json_encode($linea);
            die();
	        
	    }

	}
	
	if($_POST['operacion'] == "disminuir"){
		$_POST['monto'] = "-".$_POST['monto'];
	}

	$sql = "INSERT INTO `t_movimientos_usuarios`
			(
			`id_usuario`,
			`monto`,
			`id_usuario_registro`
			)
			VALUES
			(
			'$_POST[id_usuario]',
			'$_POST[monto]',
			'$_SESSION[id_usuario]'
			);";

    $res = mysqli_query($conexion,$sql);

    if($res){
		$linea['resultado'] = "1"; //fecha y hora ocupada
        $linea['mensaje'] = "";

    }else{
        $linea['resultado'] = "0";

        if(mysqli_errno($conexion) == 1062){
            $linea['resultado'] = "2"; //fecha y hora ocupada
            $linea['mensaje'] = "The username is already registered.";
        }else{

            $linea['mensaje'] = mysqli_error($conexion);
        }
    }

	echo json_encode($linea);
 ?>