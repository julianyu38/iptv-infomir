<?php 
	include "../conexion.php";

	session_start();

	if(!isset($_SESSION["id_usuario"])){
		header("location: index.php");
		die();
	}

	if($_SESSION["tipo_usuario"] == "V"){ //solo para administradores y super vendedores
		header("location: index.php");
		die();
	}
	

	$sql = "INSERT INTO `t_usuarios`
			(
			`nombre`,
			`usuario`,
			`pass`,
			`tipo_usuario`,
			`estatus`,
			`id_usuario_registro`
			)
			VALUES
			(
			'$_POST[nombre]',
			'$_POST[usuario]',
			'$_POST[password]',
			'$_POST[tipo_usuario]',
			'$_POST[estatus]',
			'$_SESSION[id_usuario]'
			);";

    $res = mysqli_query($conexion,$sql);

    if($res){
		$linea['resultado'] = "1"; //fecha y hora ocupada
        $linea['mensaje'] = "";

    }else{
        $linea['resultado'] = "0";

        if(mysqli_errno($conexion) == 1062){
            $linea['resultado'] = "2"; //fecha y hora ocupada
            $linea['mensaje'] = "The username is already registered.";
        }else{

            $linea['mensaje'] = mysqli_error($conexion);
        }
    }

	echo json_encode($linea);
 ?>