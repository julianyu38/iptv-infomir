<?php

include "../conexion.php";

session_start();

if (!isset($_SESSION["id_usuario"])) {
    header("location: index.php");
    die();
}

//si el usuario no es administrador se resta primero el credito de su cuenta en operaciones aumentar
//Tipos de usuario distintos de Administrador
//Osea solo Super Vendedor y Vendedor
/*
  ###############################################
 * SECCION DE AUMENTO o DISMINUCION DEL USUARIO
  ###############################################
 */
if ($_SESSION["tipo_usuario"] != "A") {

    if ($_POST['operacion'] == "aumentar") { //la operacion aumenta el saldo de un vendedor y reduce la del super vendedor
        //se consulta el saldo 
        $sql = "SELECT 
					    sum(monto) as saldo
					FROM
					    t_movimientos_usuarios
					where	
						id_usuario = '$_SESSION[id_usuario]'";

        $res = mysqli_query($conexion, $sql) or die($sql);
        $ln = mysqli_fetch_array($res);
        $saldo = $ln['saldo'];

        if ($saldo < $_POST['monto']) {
            //se detiene el proceso pues no se puede asignar saldo que no se tiene al vendedor
            $linea['resultado'] = "0"; //fecha y hora ocupada
            $linea['mensaje'] = "You do not have enough balance";
            echo json_encode($linea);
            die();
        } else {
            //se procede con el retiro del credito al Super Vendedor o Vendedor
            $monto = "-" . $_POST['monto'];
        }
    } else {
        //la operacion disminuye el saldo del vendedor y aumenta la del supervendedor
        //se consulta el saldo 
        $sql = "SELECT 
					    sum(monto) as saldo
					FROM
					    t_movimientos_clientes
					where	
						id_cliente = '$_POST[id_cliente]'";

        $res = mysqli_query($conexion, $sql);
        $ln = mysqli_fetch_array($res);
        $saldo = $ln['saldo'];

        if ($saldo < $_POST['monto']) {
            //se detiene el proceso pues no se puede restar el saldo que no se tiene al vendedor
            $linea['resultado'] = "0"; //fecha y hora ocupada
            $linea['mensaje'] = "The target seller does not have enough balance to do the operation";
            echo json_encode($linea);
            die();
        } else {
            //se procede con el aumento del credito
            $monto = $_POST['monto'];
        }
    }

    //Disminuye al usuario el monto y acreditado al cliente
    $sql = "INSERT INTO `t_movimientos_usuarios`
				(
					`id_usuario`,
					`monto`,
					`id_usuario_registro`
				)
				VALUES
				(
					'$_SESSION[id_usuario]',
					'$monto',
					'$_SESSION[id_usuario]'
				);";

    $res = mysqli_query($conexion, $sql);
    //resultado viene negativo hace un DIE
    if (!$res) {

        $linea['resultado'] = "0";
        $linea['mensaje'] = mysqli_error($conexion);

        echo json_encode($linea);
        die();
    }
}//Fin tipo de usuario distinto de Admin

/*
  ###############################################
 * SECCION DE AUMENTO o DISMINUCION DEL CLIENTE
  ###############################################
 */
//reasignacion del post monto
if ($_POST['operacion'] == "disminuir") {
    $_POST['monto'] = "-" . $_POST['monto'];
}

$sql = "INSERT INTO `t_movimientos_clientes`
			(
			`id_cliente`,
			`monto`,
			`id_usuario_registro`
			)
			VALUES
			(
			'$_POST[id_cliente]',
			'$_POST[monto]',
			'$_SESSION[id_usuario]'
			);";

$res = mysqli_query($conexion, $sql);

if ($res) {
    $linea['resultado'] = "1"; //fecha y hora ocupada
    $linea['mensaje'] = "";

    //si se llego hasta aqui ya se temrino el proceso de movimiento de credito del cliente
    //si la operacion fue aumentar se verifica si el usuario tiene dispositivos con fecha de vigencia alcanzada para reactivarlos removiendo el saldo de uno en uno

    if ($_POST['operacion'] == "aumentar") {

        //se consultan los dispositivos 

        $sql = "SELECT 
						id_dispositivo, 
						datediff( fecha_vigencia, now() ) as diferencia
					FROM 
						t_dispositivos
					WHERE
						id_cliente = '$_POST[id_cliente]' 
					ORDER BY 
						fecha_vigencia desc";

        $res2 = mysqli_query($conexion, $sql);

        while ($ln = mysqli_fetch_array($res2)) {

            if ($ln['diferencia'] < 0) { //es un dispositivo con fecha de vigencia alcanzada
                //se consulta el saldo del cliente 
                $sql = "SELECT 
		        sum(monto) as saldo
		        FROM
		        t_movimientos_clientes
			where	
		        id_cliente = '$_POST[id_cliente]'";
                $res3 = mysqli_query($conexion, $sql);
                $ln2 = mysqli_fetch_array($res3);
                $saldo = $ln2['saldo'];
                //se consulta el saldo del cliente 
                if ($saldo >= 1) {
                    $montodes = '-' . $_POST['monto'];
                    $sql = "INSERT INTO `t_movimientos_clientes`
								(
								`id_cliente`,
								`monto`,
								`id_usuario_registro`
								)
								VALUES
								(
								'$_POST[id_cliente]',
								$montodes,
								'$_SESSION[id_usuario]'
								);";

                    mysqli_query($conexion, $sql);
                    $newsaldo = $saldo * 31;
                    $fecha_vigencia = Date('Y/m/d', strtotime("+$newsaldo" . " days"));

                    $sql = "UPDATE `t_dispositivos`
								SET
								`fecha_vigencia` = '$fecha_vigencia'
								WHERE 
									`id_dispositivo` = '$ln[id_dispositivo]';";

                    mysqli_query($conexion, $sql);
                }
            } else {
                //Aqui no han vencido los creditos del cliente
                //se consulta el saldo del cliente 
                $sql = "SELECT 
							    sum(monto) as saldo
							FROM
							    t_movimientos_clientes
							where	
								id_cliente = '$_POST[id_cliente]'";

                $res3 = mysqli_query($conexion, $sql);
                $ln2 = mysqli_fetch_array($res3);
                $saldo = $ln2['saldo'];
                if ($saldo >= 1) {
                $montodes = '-' . $saldo;
                //$nsaldo = ($_POST['monto'] + $saldo);
                $sql = "INSERT INTO `t_movimientos_clientes`
								(
								`id_cliente`,
								`monto`,
								`id_usuario_registro`
								)
								VALUES
								(
								'$_POST[id_cliente]',
								$montodes,
								'$_SESSION[id_usuario]'
								);";

                mysqli_query($conexion, $sql);
                //Fecha actual vigente
                $sql = "SELECT fecha_vigencia
			FROM `t_dispositivos`
			WHERE `id_dispositivo` = '$ln[id_dispositivo]';";

                $res4 = mysqli_query($conexion, $sql);
                $ln3 = mysqli_fetch_array($res4);
                $fv = $ln3['fecha_vigencia'];
                //Fecha actual vigente
                $newsaldo = $saldo * 31;
                $fecha_vigencia = Date('Y/m/d', strtotime("$fv "."+$newsaldo" . " days"));

                    $sql = "UPDATE `t_dispositivos` 
								SET
								`fecha_vigencia` = '$fecha_vigencia'
								WHERE 
									`id_dispositivo` = '$ln[id_dispositivo]';";

                    mysqli_query($conexion, $sql);
                }//Fin if Saldo 1
            }
        }//Fin While
    }//Fin If Aumentar
} else {
    $linea['resultado'] = "0";

    if (mysqli_errno($conexion) == 1062) {
        $linea['resultado'] = "2"; //fecha y hora ocupada
        $linea['mensaje'] = "The username is already registered.";
    } else {

        $linea['mensaje'] = mysqli_error($conexion);
    }
}

echo json_encode($linea);
?>