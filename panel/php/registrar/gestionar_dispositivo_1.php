<?php

session_start();

if (!isset($_SESSION["id_usuario"])) {
    header("location: index.php");
    die();
}

include "../conexion.php";

//se verifica si existe ese dispositivo

$sql = "SELECT 
				id_dispositivo
			FROM
				t_dispositivos
			WHERE
				id_dispositivo = '$_POST[id_dispositivo]'";

$res = mysqli_query($conexion, $sql);

$num_resultado = mysqli_num_rows($res);

if ($num_resultado > 0) {
    $linea['resultado'] = "0"; //fecha y hora ocupada
    $linea['mensaje'] = "The device ID provided is already registered.";
    echo json_encode($linea);
    die();
}


if ($_POST['dispositivo_accion'] == "registrar") {

    //se consulta el saldo del cliente 
    $sql = "SELECT 
				    sum(monto) as saldo
				FROM
				    t_movimientos_clientes
				where	
					id_cliente = '$_POST[id_cliente]'";

    $res = mysqli_query($conexion, $sql);
    $ln = mysqli_fetch_array($res);
    $saldo = $ln['saldo'];

    if ($saldo < 1) {
        //se detiene el proceso pues no se puede restar el saldo que no se tiene al vendedor
        $linea['resultado'] = "0"; //fecha y hora ocupada
        $linea['mensaje'] = "The customer does not have enough balance to register the Device";
        echo json_encode($linea);
        die();
    }

    $sql = "INSERT INTO `t_movimientos_clientes`
				(
				`id_cliente`,
				`monto`,
				`id_usuario_registro`
				)
				VALUES
				(
				'$_POST[id_cliente]',
				'-1',
				'$_SESSION[id_usuario]'
				);";

    $res = mysqli_query($conexion, $sql);

    if (!$res) {
        //se detiene el proceso pues no se puede restar el saldo que no se tiene al vendedor
        $linea['resultado'] = "0"; //fecha y hora ocupada
        $linea['mensaje'] = "The operation could not be performed, take screenshot and notify your Provider.";
        echo json_encode($linea);
        die();
    }

    $fecha_vigencia = Date('Y/m/d', strtotime("+31 days"));


    $sql = "INSERT INTO `t_dispositivos`
				(
				`id_dispositivo`,
				`id_cliente`,
				`fecha_vigencia`,
				`id_usuario_registro`
				)
				VALUES
				(
				'$_POST[id_dispositivo]',
				'$_POST[id_cliente]',
				'$fecha_vigencia',
				'$_SESSION[id_usuario]'
				);";
} else {
    //actualizar
    $sql = "UPDATE `t_dispositivos`
				SET
				`id_dispositivo` = '$_POST[id_dispositivo]'
				WHERE `id_dispositivo` = '$_POST[id_dispositivo_old]';";
}

$res = mysqli_query($conexion, $sql);

if ($res) {

    $linea['resultado'] = '1';
    $linea['mensaje'] = "";
} else {
    $linea['resultado'] = '0';
    if (mysqli_errno($conexion) == 1062) {
        $linea['resultado'] = "2"; //fecha y hora ocupada
        $linea['mensaje'] = "The device ID provided is already registered.";
    } else {

        $linea['mensaje'] = mysqli_error($conexion);
    }
}

echo json_encode($linea);
die('');
?>