<?php 
	session_start();

	if(!isset($_SESSION["id_usuario"])){
		header("location: index.php");
		die();
	}

	include "../conexion.php";

  	//se registra el zona en la base de datos 
  	$sql = "INSERT INTO `t_clientes`
			(
				`nombre_cliente`,
				`telefono`,
				`correo`,
				`contenido_adultos`,
				`id_usuario_registro`
			)
			VALUES
			(
				'$_POST[nombre_cliente]',
				'$_POST[telefono]',
				'$_POST[correo]',
				'$_POST[contenido_adultos]',
				'$_SESSION[id_usuario]'
			);";

	$res = mysqli_query($conexion,$sql);

	if($res){

		$id_cliente = mysqli_insert_id($conexion);

      	$linea['resultado'] = '1';
       	$linea['mensaje'] = $id_cliente;
	}else{
		$linea['resultado'] = '0';
   		$linea['mensaje'] = mysqli_error($conexion);
	}

   	echo json_encode($linea);
   	die('');

 ?>