<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">

		<title> Clientes </title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="libs/bootstrap-3.3.7-dist/css/bootstrap.min.css">

		<!-- Bootstrap Theme CSS -->
		<link rel="stylesheet" href="libs/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">

		<!-- DataTables CSS -->
		<link rel="stylesheet" type="text/css" href="libs/dataTables/datatables.min.css"/>

		<!-- Estilos CSS -->
		<link rel="stylesheet" href="css/estilos.css">

		<!-- jQuery -->
		<script src="libs/jquery-1.12.4.min.js" type="text/javascript"></script>

 		<!-- DataTables JS -->
		<script type="text/javascript" src="libs/dataTables/datatables.min.js"></script>

		<!-- Bootstrap JS -->
		<script src="libs/bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>

        <script type="text/javascript">
            var html_cargando 	= 	'<div class="progress progress-big">'+
                                      '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                                        'Cargando...<span class="sr-only">100% Complete</span>'+
                                      '</div>'+
                                    '</div>';

            var html_procesando = 	'<div class="progress progress-big">'+
                                      '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                                        'Procesando...<span class="sr-only">100% Complete</span>'+
                                      '</div>'+
                                    '</div>';

            $(document).ready(function() {

                $("#form").on('submit', function(event) {
                    event.preventDefault();

                    $form = $(this);
                    $("#btn-enviar").prop("disabled",true);

                    $("#mensaje").html(html_procesando);
                    $.post('/add.php', $form.serialize() , function(data, textStatus, xhr) {
                        if(data.resultado == "1"){
                            $("#mensaje").html("<div class='alert alert-success'> Contenido Agregado. </div>");
                        }else{
                            $("#mensaje").html("<div class='alert alert-danger'> "+data.mensaje+" </div>");
                        }
                    },'json').always(function(){
                        $("#btn-enviar").prop("disabled",false);
                    }).fail(function(){
                        $("#mensaje").html("<div class='alert alert-danger'> Error en la comunicación, verifique su conexión a Internet y vuelva a intentarlo. </div>");
                    });

                    return false;
                });
            });

        </script>
    </head>
	<body>

	<div class="container">
            <div class="row">
                <div class="page-header">
                  <h1> Generador de menu <small> Generador de menu </small></h1>
                </div>
                <form id="form">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                          <label for="serie">Titulo</label>
                          <input type="text" class="form-control" id="serie" name="serie" placeholder="" required="">
                          <p class="help-block"> Ingrese el titulo de la pelicula sin acentos </p>
                        </div>
                    </div>
					<div class="form-group">
                          <br><label for="sinopsis">sinopsis</label>
                          <input type="text" class="form-control" id="sin" name="sin" placeholder="" required="">
                          <p class="help-block"> Ingrese la sinopsis sin acentos </p>
                        </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="formato">Formato</label>
                            <select name="formato" id="formato" class="form-control" required="">
                                <option value="ts"> ts </option>
                                <option value="hls"> hls </option>
								<option value="mkv"> mkv </option>
								<option value="mp4"> mp4 </option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix hidden-md hidden-lg"></div>
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-3 col-md-offset-0">
                        <div class="form-group">
                            <label for="" class="hidden-xs hidden-sm"> &nbsp; </label>
                            <button type="submit" id="btn-enviar"  class="btn btn-primary form-control" >
                                Enviar
                            </button>
                        </div>
                    </div>
                    <div class="col-xs-12" id="mensaje">
                    </div>
                </form>
            </div>
        </div>
	
	
	
	
	
	
	
	
			<footer class="footer">
        <p>&copy; 2018 PROTVPLUS</p>
      </footer>
	  </center>
    </body>
</html>
