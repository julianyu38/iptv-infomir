<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">

		<title> Clientes </title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="libs/bootstrap-3.3.7-dist/css/bootstrap.min.css">

		<!-- Bootstrap Theme CSS -->
		<link rel="stylesheet" href="libs/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">

		<!-- DataTables CSS -->
		<link rel="stylesheet" type="text/css" href="libs/dataTables/datatables.min.css"/>

		<!-- Estilos CSS -->
		<link rel="stylesheet" href="css/estilos.css">

		<!-- jQuery -->
		<script src="libs/jquery-1.12.4.min.js" type="text/javascript"></script>

 		<!-- DataTables JS -->
		<script type="text/javascript" src="libs/dataTables/datatables.min.js"></script>

		<!-- Bootstrap JS -->
		<script src="libs/bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>

        <script type="text/javascript">
            var html_cargando 	= 	'<div class="progress progress-big">'+
                                      '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                                        'Cargando...<span class="sr-only">100% Complete</span>'+
                                      '</div>'+
                                    '</div>';

            var html_procesando = 	'<div class="progress progress-big">'+
                                      '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                                        'Procesando...<span class="sr-only">100% Complete</span>'+
                                      '</div>'+
                                    '</div>';

            $(document).ready(function() {

                $("#form").on('submit', function(event) {
                    event.preventDefault();

                    $form = $(this);
                    $("#btn-enviar").prop("disabled",true);

                    $("#mensaje").html(html_procesando);
                    $.post('php/actualizar/actualizar_acceso_contenido_para_adultos.php', $form.serialize() , function(data, textStatus, xhr) {
                        if(data.resultado == "1"){
                            $("#mensaje").html("<div class='alert alert-success'> Cambio realizado, favor de reiniciar el roku. </div>");
                        }else{
                            $("#mensaje").html("<div class='alert alert-danger'> "+data.mensaje+" </div>");
                        }
                    },'json').always(function(){
                        $("#btn-enviar").prop("disabled",false);
                    }).fail(function(){
                        $("#mensaje").html("<div class='alert alert-danger'> Error en la comunicación, verifique su conexión a Internet y vuelva a intentarlo. </div>");
                    });

                    return false;
                });
            });

        </script>
    </head>
	<body>
	<?php include "php/include/navbar2.php" ?>
	<script>alert("Se recomienda restaurar a fabrica el dispositivo y crear cuenta nueva de roku antes de instalar el paquete")</script>';
        <div class="container">
            <div class="row">
                <div class="page-header">
                  <h1> Instalacion modo DEV <small> Guia de usuario </small></h1>
                </div>
				
				 <div class="container">
                      <div class="header clearfix">
       <center>
	   <h3 class="text-muted"><img src=""><br>Instalacion de CANAL ROKU MODO DEV</h3>
      </div>


      <div class="row marketing">
       <center> <h2><a class="btn btn-primary btn-lg active" target="_blank" href="">PROTVPLUS</a></h2>
		



          <hr>

          <h3>Tutorial para la instalacion del canal</h3>
          <p>Este tutorial fue probado en ROKU XPRESS</p>

        <h4>1.- Digitar esta secuencia de teclas para activar el modo dev.</h4>
        <img src="screenshot001.png" class="img-thumbnail">


        <h4>2.- Haz todo eso y deberías ver la pantalla secreta de Desarrollador, escriba la dirección IP y el nombre de usuario que se ven aquí, porque los necesitará más adelante, le damos en "Enable installer and restart"</h4>
		
        <img src="screenshot002.png" class="img-thumbnail">

        <h4>3.- Se le preguntará si está de acuerdo con el Acuerdo de licencia de SKD, le damos en "I Agree"</h4>
        <img src="screenshot003.png" class="img-thumbnail">

        <h4>4.- Se le pedirá que elija una contraseña de desarrollo del servidor web <strong>!!!ATENCION!! "no colocar 1234 intenta colocar contraseña con numeros y letras""</strong>, tome nota de la contraseña que configuró, idealmente en el mismo lugar donde tomó nota de su dirección IP y nombre de usuario anteriormente. Tu Roku ahora se reiniciará. Una vez que se inicia, puede acceder al Modo desarrollador..</h4>
        <img src="screenshot004.png" class="img-thumbnail">

        <h4>5.- En una computadora conectada a la misma red que su Roku, abra su navegador web. Pegue la dirección IP que anotó anteriormente en la barra de la URL, luego presione Enter, y se le pedirá su nombre de usuario y contraseña.</h4>
        <img src="screenshot005.png" class="img-thumbnail">

        <h4>6.- Haga clic en el botón "Subir", luego apunte su navegador hacia su archivo ZIP.</h4>
        <img src="screenshot006.png" class="img-thumbnail">

        <h4>7.- Haga clic en "Install" y comenzará el proceso de instalación. Cuando esté hecho, tu aplicación se abrirá instantáneamente en el Roku..</h4>
        <img src="screenshot007.png" class="img-thumbnail">

        <h4>8.- Si la instalacion es satisfactoria veremos la siguiente imagen.</h4>
        <img src="screenshot008.png" class="img-thumbnail">

        <h4>9.- Y finalmente PROTVPLUS estará instalado en ROKU</h4>


        <h4>10.- Solo queda ingresar para empezar a disfrutar del contenido de PROTVPLUS!</h4>


        

      </div>

      

				</div>
        </div>
		
		<footer class="footer">
        <p>&copy; 2018 PROTVPLUS</p>
      </footer>
	  </center>
    </body>
</html>
