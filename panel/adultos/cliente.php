<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">

		<title> Clientes </title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="libs/bootstrap-3.3.7-dist/css/bootstrap.min.css">

		<!-- Bootstrap Theme CSS -->
		<link rel="stylesheet" href="libs/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">

		<!-- DataTables CSS -->
		<link rel="stylesheet" type="text/css" href="libs/dataTables/datatables.min.css"/>

		<!-- Estilos CSS -->
		<link rel="stylesheet" href="css/estilos.css">

		<!-- jQuery -->
		<script src="libs/jquery-1.12.4.min.js" type="text/javascript"></script>

 		<!-- DataTables JS -->
		<script type="text/javascript" src="libs/dataTables/datatables.min.js"></script>

		<!-- Bootstrap JS -->
		<script src="libs/bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>

        <script type="text/javascript">
            var html_cargando 	= 	'<div class="progress progress-big">'+
                                      '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                                        'Cargando...<span class="sr-only">100% Complete</span>'+
                                      '</div>'+
                                    '</div>';

            var html_procesando = 	'<div class="progress progress-big">'+
                                      '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                                        'Procesando...<span class="sr-only">100% Complete</span>'+
                                      '</div>'+
                                    '</div>';

            $(document).ready(function() {

                $("#form").on('submit', function(event) {
                    event.preventDefault();

                    $form = $(this);
                    $("#btn-enviar").prop("disabled",true);

                    $("#mensaje").html(html_procesando);
                    $.post('php/actualizar/actualizar_acceso_contenido_para_adultos.php', $form.serialize() , function(data, textStatus, xhr) {
                        if(data.resultado == "1"){
                            $("#mensaje").html("<div class='alert alert-success'> Cambio realizado, favor de reiniciar el roku. </div>");
                        }else{
                            $("#mensaje").html("<div class='alert alert-danger'> "+data.mensaje+" </div>");
                        }
                    },'json').always(function(){
                        $("#btn-enviar").prop("disabled",false);
                    }).fail(function(){
                        $("#mensaje").html("<div class='alert alert-danger'> Error en la comunicación, verifique su conexión a Internet y vuelva a intentarlo. </div>");
                    });

                    return false;
                });
            });

        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="page-header">
                  <h1> Control parental <small> Acceso a contenido para adultos </small></h1>
                </div>
                <form id="form">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                          <label for="correo">Correo</label>
                          <input type="text" class="form-control" id="correo" name="correo" placeholder="" required="">
                          <p class="help-block"> si no proporciono un correo use su ID del dispositivo </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="contenido_adultos">Contenido para adultos</label>
                            <select name="contenido_adultos" id="contenido_adultos" class="form-control" required="">
                                <option value="No"> No </option>
                                <option value="Si"> Si </option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix hidden-md hidden-lg"></div>
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-3 col-md-offset-0">
                        <div class="form-group">
                            <label for="" class="hidden-xs hidden-sm"> &nbsp; </label>
                            <button type="submit" id="btn-enviar"  class="btn btn-primary form-control" >
                                Enviar
                            </button>
                        </div>
                    </div>
                    <div class="col-xs-12" id="mensaje">
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
