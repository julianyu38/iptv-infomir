<?php 
	session_start();

	if(!isset($_SESSION["id_usuario"])){
		header("location: index.php");
		die();
	}

	if($_SESSION["tipo_usuario"] == "V"){ //solo para administradores y super vendedores
		header("location: index.php");
		die();
	}

	include "php/conexion.php";
 ?>
<!DOCTYPE html>
	<html>
	<body background="http://skylinetv.net/1.png">
	<body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
		
		<title> REVENDEDORES </title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="libs/bootstrap-3.3.7-dist/css/bootstrap.min.css">

		<!-- Bootstrap Theme CSS -->
		<link rel="stylesheet" href="libs/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">
	
		<!-- DataTables CSS -->
		<link rel="stylesheet" type="text/css" href="libs/dataTables/datatables.min.css"/>
 	
		<!-- Estilos CSS -->
		<link rel="stylesheet" href="css/estilos.css">
		
		<!-- jQuery -->
		<script src="libs/jquery-1.12.4.min.js" type="text/javascript"></script>

 		<!-- DataTables JS -->
		<script type="text/javascript" src="libs/dataTables/datatables.min.js"></script>
		
		<!-- Bootstrap JS -->
		<script src="libs/bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script type="text/javascript">

			var html_cargando 	= 	'<div class="progress progress-big">'+
									  '<div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
									    'Cargando...<span class="sr-only">100% Complete</span>'+
									  '</div>'+
									'</div>';

			var html_procesando = 	'<div class="progress progress-big">'+
									  '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
									    'Procesando...<span class="sr-only">100% Complete</span>'+
									  '</div>'+
									'</div>';
			
			var id_usuario 			= 0;
			var form_usuario_accion 	= "registrar";

			$(document).ready(function() {

				$('#menu_usuario').addClass('active');
				
				actualizar_tabla_usuarios();

				

				$('#monto').on('keypress',  function(event) {
					$(this).parent().parent().removeClass('has-error');
				});

				$('#ventana_usuario').on('hidden.bs.modal', function(event) {
					event.preventDefault();
					
					$('#form_usuario')[0].reset();
					form_usuario_accion 	= "registrar";
					$('#mensaje_usuario').html("");
					$('#btn-enviar-usuario').text("Registrar");
				});
				
				$('#ventana_gestion_creditos').on('hidden.bs.modal', function(event) {
					$('#monto').val("");
					$('#mensaje_monto').html("");

					
				});
				
				$('#form_usuario').on('submit', function(event) {
					event.preventDefault();

					$('#btn-enviar-usuario').prop("disabled",true);

					$('#mensaje_usuario').html(html_procesando);

					var url = "php/registrar/registrar_usuario.php";
					if(form_usuario_accion != "registrar"){
						url = "php/actualizar/actualizar_usuario.php";
					}

					var formData = new FormData( $('#form_usuario')[0] );

					formData.append('id_usuario',id_usuario);

					$.ajax({
						url: url,
						type: 'POST',
						dataType: 'json',
						data: formData,
						processData: false,
						contentType: false  
					})
					.done(function(data) {
						if(data.resultado == "1"){

							actualizar_tabla_usuarios();

							//Registro exitoso
							if(form_usuario_accion == "registrar"){
								$('#mensaje_usuario').html( "<div class='alert alert-success'> Registro exitoso </div>" );
							}else{
								$('#mensaje_usuario').html( "<div class='alert alert-success'> Actualizacion exitoza </div>" );
							}

							setTimeout(function(){
								$('#ventana_usuario').modal("hide");
							},1000);

						}else{
							$('#mensaje_usuario').html( "<div class='alert alert-danger'> "+data.mensaje+"</div>" );
						}
					})
					.fail(function() {
						console.log("error form_usuario");
						$('#mensaje_usuario').html( "<div class='alert alert-danger'> Error en la comunicación, verifique su conexion a Internet.</div>" );
					})
					.always(function() {
						console.log("complete form_usuario");
						$('#btn-enviar-usuario').prop("disabled",false);
					});
					
					return false;
				});		

				consultar_saldo();
				
			});//Termina Ready

			function actualizar_tabla_usuarios(){
				$('#contenedor_usuarios').html(html_cargando);

				$.post('tabla/tabla_usuarios.php', function(data, textStatus, xhr) {
					
					$('#contenedor_usuarios').html(data);

					$('.editar_usuario').click(function(event) {
						var datos = $(this).parent().parent();

						id_usuario = datos.data("id");
						nombre = datos.data("nombre");
						usuario = datos.data("usuario");
						password = datos.data("pass");
						tipo_usuario = datos.data("tipo");
						estatus = datos.data("estatus");

						$('#nombre').val(nombre);
						$('#usuario').val(usuario);
						$('#password').val(password);
						$('#tipo_usuario').val(tipo_usuario);
						$('#estatus').val(estatus);

						form_usuario_accion = "actualizar";

						$('#btn-enviar-usuario').text("Actualizar");
						
						$('#ventana_usuario').modal("show");

					});

					$('.eliminar_usuario').click(function(event) {
						var datos = $(this).parent().parent();

						id_usuario = datos.data("id");
						nombre = datos.data("nombre");
						

						var res = confirm("¿Desea eliminar el usuario: "+nombre+" ?");

						if(res){
							
							$.post('php/eliminar/eliminar_usuario.php', {id_usuario: id_usuario}, function(data, textStatus, xhr) {
								if(data == "1"){
									alert("Usuario eliminado");
									actualizar_tabla_usuarios();
								}else{
									alert(data);
								}
							}).fail(function(){
								alert("Error en la comunicación, verifique su conexión a Internet.");
							});
						}

					});

					$('.gestion_creditos').click(function(event) {
						var datos = $(this).parent().parent();

						id_usuario = datos.data("id");
						nombre = datos.data("nombre");

						$('#ventana_gestion_creditos').modal("show");

						actualizar_tabla_montos();
					});

					$('#tabla_usuarios').DataTable({
						"order": [],
						"language": {
						    "sProcessing":     "Procesando...",
						    "sLengthMenu":     "Mostrar _MENU_ registros",
						    "sZeroRecords":    "No se encontraron resultados",
						    "sEmptyTable":     "Ningún dato disponible en esta tabla",
						    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
						    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
						    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
						    "sInfoPostFix":    "",
						    "sSearch":         "Buscar:",
						    "sUrl":            "",
						    "sInfoThousands":  ",",
						    "sLoadingRecords": "Cargando...",
						    "oPaginate": {
						        "sFirst":    "Primero",
						        "sLast":     "Último",
						        "sNext":     "Siguiente",
						        "sPrevious": "Anterior"
						    },
						    "oAria": {
						        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
						    }
						}
					});

				}).fail(function(){
					$('#contenedor_usuarios').html('<div class="alert alert-danger"> Error en la comunicación, verifique su conexion a Internet. </div>');
				});
			}//Termina actualizar tabla de usuarios

			function actualizar_tabla_montos(){
				$('#contenedor_movimientos').html(html_cargando);

				$.post('tabla/tabla_movimientos_usuarios.php',{id_usuario, id_usuario}, function(data, textStatus, xhr) {
					$('#contenedor_movimientos').html(data);
				}).fail(function(){
					$('#contenedor_movimientos').html('<div class="alert alert-danger"> Error en la comunicación, verifique su conexion a Internet. </div>');
				});
			}

			function gestion_creditos(operacion){

				var monto = $('#monto').val().trim();

				$('#monto').val(monto);

				if(monto == ""){
					$('#monto').focus();
				}else{
					//no esta vacio el campo

					if(isNaN(monto)){
						//NO es un numero
						$('#monto').focus().parent().parent().addClass('has-error');
					}else{
						//es un numero
						if(monto <= 0){
							//el numero es negativo o igual a 0
							$('#monto').focus().parent().parent().addClass('has-error');
						}else{
							//el numero es positivo y diferente a 0
							//se procede con la operacion
							$('.btn-gestion-credito').prop("disabled",true);

							$('#mensaje_monto').html(html_procesando);

							$.post('php/registrar/registrar_movimiento_usuarios.php', {operacion: operacion, monto: monto, id_usuario: id_usuario}, function(data, textStatus, xhr) {
								$('#monto').val("");

								if(data.resultado == "1"){
									$('#mensaje_monto').html("<div class='alert alert-success'> Operación exitosa. </div>");

									actualizar_tabla_usuarios();
									consultar_saldo();

									setTimeout(function(){
										$('#mensaje_monto').html("");
									},1500);

								}else{
									$('#mensaje_monto').html("<div class='alert alert-danger'> "+ data.mensaje+" </div>");
								}

								actualizar_tabla_montos();
								
							},'json').fail(function(){
								$('#mensaje_monto').html("<div class='alert alert-danger'> Error en la comunicación, verifique su conexión a Internet. </div>");
							}).always(function(){
								$('.btn-gestion-credito').prop("disabled",false);
							});
						}

					}
				}
			}

			function consultar_saldo(){
				//se consulta el saldo del usuario
				$.post('php/consultar/consulta_saldo.php', function(data, textStatus, xhr) {
					$('.saldo').text("Usted cuenta con: "+data.saldo+" Créditos. ");
				},'json');
			}
			
		</script>
		<CENTER><img src="http://167.114.144.169/logon.png" width="500" height="179" alt=""></td></CENTER> 
	</head>
	<body>
		<?php include "php/include/navbar.php" ?>
		<div class="container">
			<div class="page-header">
				<h1> REVENDEDORES <span class="saldo"></span> </h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button class="btn btn-info" data-toggle="modal" data-target="#ventana_usuario">
						Registrar nuevo revendedor
					</button>
				</div>
				<div class="col-xs-12 mt-15 table-responsive" id="contenedor_usuarios">
					
				</div>
			</div>
		</div>

		<!-- Modal usuario  -->
		<div class="modal fade" id="ventana_usuario" tabindex="-1" role="dialog">
		  	<div class="modal-dialog modal-lg">
		    	<div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        	<h4 class="modal-title">Registrar nuevo revendedor</h4>
			      	</div>
			      	<form id="form_usuario">
				      	<div class="modal-body">
				        	<div class="row">
				        		<div class="col-xs-12 col-md-6">
				        			<div class="form-group">
									    <label for="nombre">Nombre</label>
									    <input name="nombre" id="nombre" type="text" class="form-control" required="">
									</div>
				        		</div>
				        		<div class="col-xs-12 col-md-6">
				        			<div class="form-group">
									    <label for="usuario">Usuario</label>
									    <input name="usuario" id="usuario" type="text" class="form-control" required="">
									</div>
				        		</div>
				        		<div class="col-xs-12 col-md-6">
				        			<div class="form-group">
									    <label for="password">Contraseña</label>
									    <input name="password" id="password" type="text" class="form-control" minlength="8" maxlength="15" required="">
									</div>
				        		</div>
				        		<div class="col-xs-12 col-md-6">
				        			<div class="form-group">
									    <label for="tipo_usuario">Tipo de usuario</label>
									    <select name="tipo_usuario" id="tipo_usuario"  class="form-control">
									    	<?php 

												if($_SESSION['tipo_usuario'] == "S"){
													//Super vendedor
													echo '<option value="V"> Vendedor </option>';
												}else{
													echo '<option value="S"> Super Vendedor </option>';
													echo '<option value="V"> Vendedor </option>';
												} 
											?>
									    </select>
									</div>
				        		</div>
				        		<div class="col-xs-12 col-md-6">
				        			<div class="form-group">
									    <label for="estatus">Estatus</label>
									    <select name="estatus" id="estatus"  class="form-control">
									    	<option value="A"> Activo </option>
									    	<option value="I"> Inactivo </option>
									    </select>
									</div>
				        		</div>
				        		
				        		<div class="col-xs-12" id="mensaje_usuario">
				        		</div>
				        	</div>
				      	</div>
				      	<div class="modal-footer">
				        	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				        	<button type="submit" class="btn btn-info" id="btn-enviar-usuario"> Registrar </button>
				      	</div>
			      	</form>
		    	</div>
		  	</div>
		</div>

		

		<!-- Modal usuario  -->
		<div class="modal fade" id="ventana_gestion_creditos" tabindex="-1" role="dialog">
		  	<div class="modal-dialog modal-sm">
		    	<div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        	<h4 class="modal-title">Gestión de créditos</h4>
			      	</div>
			      	
				    <div class="modal-body">
			        	<div class="row">
			        		<div class="col-xs-12">
			        			<div class="form-group">
								    <label class="label-control" for="monto">Monto</label>
								    <div class="input-group">
								    	<input name="monto" id="monto" type="text" class="form-control">
								    	<span class="input-group-addon"> Créditos </span>
								    </div>
								</div>
			        		</div>
			        		<div class="col-xs-12" id="mensaje_monto"></div>
			        		<div class="col-xs-12 col-sm-6 col-left">
			        			<button class="btn btn-info btn-gestion-credito" onclick="gestion_creditos('aumentar')"> Aumentar  </button>
			        		</div>
			        		<div class="col-xs-12 col-sm-6 col-right">
			        			<button class="btn btn-warning btn-gestion-credito" onclick="gestion_creditos('disminuir')"> Disminuir </button>
			        		</div>
			        		<div class="col-xs-12"> 
			        			<hr>
			        		</div>
			        		<div class="col-xs-12"> 
			        			<h4>
			        				Movimientos
			        			</h4>
			        		</div>
			        		<div class="col-xs-12" id="contenedor_movimientos">
			        			
			        		</div>
			        	</div>
			      	</div>
			      	<div class="modal-footer">
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			      	</div>
		      	
		    	</div>
		  	</div>
		</div>


	</body>
</html>
