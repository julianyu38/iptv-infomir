<?php 

	session_start();

	if(!isset($_SESSION["id_usuario"])){
		header("location: index.php");
		die();
	}

	include "php/conexion.php";
	
 ?>
<!DOCTYPE html>
	<html>
	<body background="http://skylinetv.net/1.png">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
		
		<title> PANEL PARA CLIENTES </title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="libs/bootstrap-3.3.7-dist/css/bootstrap.min.css">

		<!-- Bootstrap Theme CSS -->
		<link rel="stylesheet" href="libs/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">
	
		<!-- DataTables CSS -->
		<link rel="stylesheet" type="text/css" href="libs/dataTables/datatables.min.css"/>
 	
		<!-- Estilos CSS -->
		<link rel="stylesheet" href="css/estilos.css">
		
		<!-- jQuery -->
		<script src="libs/jquery-1.12.4.min.js" type="text/javascript"></script>

 		<!-- DataTables JS -->
		<script type="text/javascript" src="libs/dataTables/datatables.min.js"></script>
		
		<!-- Bootstrap JS -->
		<script src="libs/bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script type="text/javascript">

			var html_cargando 	= 	'<div class="progress progress-big">'+
									  '<div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
									    'Cargando...<span class="sr-only">100% Complete</span>'+
									  '</div>'+
									'</div>';

			var html_procesando = 	'<div class="progress progress-big">'+
									  '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
									    'Procesando...<span class="sr-only">100% Complete</span>'+
									  '</div>'+
									'</div>';
			
			var id_cliente 			= 0;

			var id_dispositivo_old 		= "";
			var form_cliente_accion 	= "registrar";
			var dispositivo_accion		= "registrar";

			$(document).ready(function() {

				$('#menu_cliente').addClass('active');
				
				actualizar_tabla_clientes();

				$('#monto').on('keypress',  function(event) {
					$(this).parent().parent().removeClass('has-error');
				});


				$('#ventana_cliente').on('hidden.bs.modal', function(event) {
					event.preventDefault();
					
					$('#form_cliente')[0].reset();
					form_cliente_accion 	= "registrar";
					$('#mensaje_cliente').html("");
					$('#btn-enviar-cliente').text("Registrar");
				});

				$('#ventana_gestion_creditos').on('hidden.bs.modal', function(event) {
					$('#monto').val("");
					$('#mensaje_monto').html("");
				});

				$('#ventana_gestion_dispositivos').on('hidden.bs.modal', function(event) {
					$('#id_dispositivo').val("");
					$('#mensaje_dispositivo').html("");
					dispositivo_accion = "registrar";
					$('#btn-gestion-dispositivos').html("Agregar");
				});


				
				
				$('#form_cliente').on('submit', function(event) {
					event.preventDefault();

					$('#btn-enviar-cliente').prop("disabled",true);

					$('#mensaje_cliente').html(html_procesando);

					var url = "php/registrar/registrar_cliente.php";
					if(form_cliente_accion != "registrar"){
						url = "php/actualizar/actualizar_cliente.php";
					}

					var formData = new FormData( $('#form_cliente')[0] );

					formData.append('id_cliente',id_cliente);

					$.ajax({
						url: url,
						type: 'POST',
						dataType: 'json',
						data: formData,
						processData: false,
						contentType: false  
					})
					.done(function(data) {
						if(data.resultado == "1"){

							actualizar_tabla_clientes();

							//Registro exitoso
							if(form_cliente_accion == "registrar"){
								$('#mensaje_cliente').html( "<div class='alert alert-success'> Registro exitoso </div>" );
							}else{
								$('#mensaje_cliente').html( "<div class='alert alert-success'> Actualizacion exitoza </div>" );
							}

							setTimeout(function(){
								$('#ventana_cliente').modal("hide");
							},1000);

						}else{
							$('#mensaje_cliente').html( "<div class='alert alert-danger'> "+data.mensaje+"</div>" );
						}
					})
					.fail(function() {
						console.log("error form_cliente");
						$('#mensaje_cliente').html( "<div class='alert alert-danger'> Error en la comunicación, verifique su conexion a Internet.</div>" );
					})
					.always(function() {
						console.log("complete form_cliente");
						$('#btn-enviar-cliente').prop("disabled",false);
					});
					
					return false;
				});		

				$('#btn-gestion-dispositivos').click(function(event) {
					var id_dispositivo = $('#id_dispositivo').val().trim();

					$('#id_dispositivo').val(id_dispositivo);

					if(id_dispositivo == ""){
						$('#id_dispositivo').focus();
					}else{
						//se procede con la operacion
						$('#btn-gestion-dispositivos').prop("disabled",true);

						$('#mensaje_dispositivo').html(html_procesando);

						$.post('php/registrar/gestionar_dispositivo.php', {dispositivo_accion: dispositivo_accion, id_dispositivo: id_dispositivo, id_cliente: id_cliente, id_dispositivo_old: id_dispositivo_old}, function(data, textStatus, xhr) {
							
							if(data.resultado == "1"){

								$('#id_dispositivo').val("");

								$('#mensaje_dispositivo').html("<div class='alert alert-success'> Operación exitosa. </div>");

								actualizar_tabla_clientes();
								
								setTimeout(function(){
									$('#mensaje_dispositivo').html("");

									dispositivo_accion = "registrar";

									$('#btn-gestion-dispositivos').html("Agregar");
								},1500);

							}else{
								$('#mensaje_dispositivo').html("<div class='alert alert-danger'> "+ data.mensaje+" </div>");
							}

							actualizar_tabla_dispositivos();
							
						},'json').fail(function(){
							$('#mensaje_dispositivo').html("<div class='alert alert-danger'> Error en la comunicación, verifique su conexión a Internet. </div>");
						}).always(function(){
							$('#btn-gestion-dispositivos').prop("disabled",false);
						});
					
					}
				});

				consultar_saldo();
				
			});//Termina Ready

			function actualizar_tabla_clientes(){
				$('#contenedor_clientes').html(html_cargando);

				$.post('tabla/tabla_clientes.php', function(data, textStatus, xhr) {
					
					$('#contenedor_clientes').html(data);

					$('.editar_cliente').click(function(event) {
						var datos = $(this).parent().parent().parent().parent();

						console.dir(datos);

						id_cliente = datos.data("id");
						nombre = datos.data("nombre");
						telefono = datos.data("telefono");
						correo = datos.data("correo");
						adultos = datos.data("adultos");

						$('#nombre_cliente').val(nombre);
						$('#telefono').val(telefono);
						$('#correo').val(correo);
						$('#contenido_adultos').val(adultos);

						form_cliente_accion = "actualizar";

						$('#btn-enviar-cliente').text("Actualizar");
						
						$('#ventana_cliente').modal("show");

					});

					
					$('.eliminar_cliente').click(function(event) {
						var datos = $(this).parent().parent().parent().parent();

						id_cliente = datos.data("id");
						nombre = datos.data("nombre");

						var res = confirm("¿Desea eliminar al cliente: "+nombre+" ?");

						if(res){
							
							$.post('php/eliminar/eliminar_cliente.php', {id_cliente: id_cliente}, function(data, textStatus, xhr) {
								if(data == "1"){
									alert("Cliente eliminado.");
									actualizar_tabla_clientes();
								}else{
									alert(data);
								}
							}).fail(function(){
								alert("Error en la comunicación, verifique su conexión a Internet.");
							});
						}

					});

					$('.gestion_creditos').click(function(event) {
						var datos = $(this).parent().parent().parent().parent();

						id_cliente = datos.data("id");
						nombre = datos.data("nombre");

						$('#ventana_gestion_creditos').modal("show");

						actualizar_tabla_montos();
					});

					$('.gestion_dispositivos').click(function(event) {
						var datos = $(this).parent().parent().parent().parent();

						id_cliente = datos.data("id");

						$('#ventana_gestion_dispositivos').modal("show");

						actualizar_tabla_dispositivos();
					});
					

					$('#tabla_clientes').DataTable({
						"order": [],
						"language": {
						    "sProcessing":     "Procesando...",
						    "sLengthMenu":     "Mostrar _MENU_ registros",
						    "sZeroRecords":    "No se encontraron resultados",
						    "sEmptyTable":     "Ningún dato disponible en esta tabla",
						    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
						    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
						    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
						    "sInfoPostFix":    "",
						    "sSearch":         "Buscar:",
						    "sUrl":            "",
						    "sInfoThousands":  ",",
						    "sLoadingRecords": "Cargando...",
						    "oPaginate": {
						        "sFirst":    "Primero",
						        "sLast":     "Último",
						        "sNext":     "Siguiente",
						        "sPrevious": "Anterior"
						    },
						    "oAria": {
						        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
						    }
						}
					});

				}).fail(function(){
					$('#contenedor_clientes').html('<div class="alert alert-danger"> Error en la comunicación, verifique su conexion a Internet. </div>');
				});
			}//Termina actualizar tabla de clientes

			function actualizar_tabla_montos(){
				$('#contenedor_movimientos').html(html_cargando);

				$.post('tabla/tabla_movimientos_clientes.php',{id_cliente, id_cliente}, function(data, textStatus, xhr) {
					$('#contenedor_movimientos').html(data);
				}).fail(function(){
					$('#contenedor_movimientos').html('<div class="alert alert-danger"> Error en la comunicación, verifique su conexion a Internet. </div>');
				});
			}

			function gestion_creditos(operacion){

				var monto = $('#monto').val().trim();

				$('#monto').val(monto);

				if(monto == ""){
					$('#monto').focus();
				}else{
					//no esta vacio el campo

					if(isNaN(monto)){
						//NO es un numero
						$('#monto').focus().parent().parent().addClass('has-error');
					}else{
						//es un numero
						if(monto <= 0){
							//el numero es negativo o igual a 0
							$('#monto').focus().parent().parent().addClass('has-error');
						}else{
							//el numero es positivo y diferente a 0
							//se procede con la operacion
							$('.btn-gestion-credito').prop("disabled",true);

							$('#mensaje_monto').html(html_procesando);

							$.post('php/registrar/registrar_movimiento_clientes.php', {operacion: operacion, monto: monto, id_cliente: id_cliente}, function(data, textStatus, xhr) {
								$('#monto').val("");

								if(data.resultado == "1"){
									$('#mensaje_monto').html("<div class='alert alert-success'> Operación exitosa. </div>");

									actualizar_tabla_clientes();
									consultar_saldo();

									setTimeout(function(){
										$('#mensaje_monto').html("");
									},1500);

								}else{
									$('#mensaje_monto').html("<div class='alert alert-danger'> "+ data.mensaje+" </div>");
								}

								actualizar_tabla_montos();
								
							},'json').fail(function(){
								$('#mensaje_monto').html("<div class='alert alert-danger'> Error en la comunicación, verifique su conexión a Internet. </div>");
							}).always(function(){
								$('.btn-gestion-credito').prop("disabled",false);
							});
						}

					}

				}

			}

			function consultar_saldo(){
				//se consulta el saldo del usuario
				$.post('php/consultar/consulta_saldo.php', function(data, textStatus, xhr) {
					$('.saldo').text("Usted cuenta con: "+data.saldo+" Créditos. ");
				},'json');
			}

			function actualizar_tabla_dispositivos(){
				$('#contenedor_dispositivos').html(html_cargando);

				$.post('tabla/tabla_dispositivos.php',{ id_cliente, id_cliente }, function(data, textStatus, xhr) {
					$('#contenedor_dispositivos').html(data);

					$('.editar_dispositivo').click(function(event) {
						var datos = $(this).parent().parent();

						id_dispositivo_old = datos.data("id");

						$('#id_dispositivo').val(id_dispositivo_old);

						dispositivo_accion = "actualizar";

						$('#btn-gestion-dispositivos').html("Actualizar");

					});

					$('.eliminar_dispositivo').click(function(event) {
						var datos = $(this).parent().parent();

						id_dispositivo = datos.data("id");

						var res = confirm("¿Desea eliminar el dispositivo: "+id_dispositivo+" ?");

						if(res){
							
							$.post('php/eliminar/eliminar_dispositivo.php', {id_dispositivo: id_dispositivo}, function(data, textStatus, xhr) {
								if(data == "1"){
									alert("Dispositivo eliminado.");
									actualizar_tabla_dispositivos();
									actualizar_tabla_clientes();
								}else{
									alert(data);
								}
							}).fail(function(){
								alert("Error en la comunicación, verifique su conexión a Internet.");
							});
						}						

					});

					
				}).fail(function(){
					$('#contenedor_dispositivos').html('<div class="alert alert-danger"> Error en la comunicación, verifique su conexion a Internet. </div>');
				});
			}
			
		</script>
		<CENTER><img src="http://167.114.144.169/logon.png" width="500" height="179" alt=""></td></CENTER> 	
	</head>
	<body>
		<?php include "php/include/navbar.php" ?>
		<div class="container">
			<div class="page-header">
				<h1> CLIENTES <span class="saldo"></span></h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button class="btn btn-info" data-toggle="modal" data-target="#ventana_cliente">
						Registrar nuevo cliente
					</button>
				</div>
				<div class="col-xs-12 mt-15 table-responsive" id="contenedor_clientes">
					
				</div>
			</div>
		</div>

		<!-- Modal cliente  -->
		<div class="modal fade" id="ventana_cliente" tabindex="-1" role="dialog">
		  	<div class="modal-dialog modal-lg">
		    	<div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        	<h4 class="modal-title">Registrar Cliente nuevo</h4>
			      	</div>
			      	<form id="form_cliente">
				      	<div class="modal-body">
				        	<div class="row">
				        		<div class="col-xs-12 col-md-6">
				        			<div class="form-group">
									    <label for="nombre_cliente">Nombre de Cliente</label>
									    <input name="nombre_cliente" id="nombre_cliente" type="text" class="form-control" placeholder="Nombre de Usuario" required="">
									</div>
				        		</div>
				        		<div class="col-xs-12 col-md-6">
				        			<div class="form-group">
									    <label for="telefono">Telefono</label>
									    <input name="telefono" id="telefono" type="text" class="form-control" required="">
									</div>
				        		</div>
				        		<div class="col-xs-12 col-md-6">
				        			<div class="form-group">
									    <label for="correo">Correo</label>
									    <input name="correo" id="correo" type="email" class="form-control" required="">
									</div>
				        		</div>
				        		<div class="col-xs-12 col-md-6">
				        			<div class="form-group">
									    <label for="contenido_adultos">Contenido para adultos</label>
									    <select name="contenido_adultos" id="contenido_adultos" class="form-control" required="">
									    	<option value="No"> No </option>
									    	<option value="Si"> Si </option>
									    </select>
									</div>
				        		</div>
				        		<div class="col-xs-12" id="mensaje_cliente">
				        		</div>
				        	</div>
				      	</div>
				      	<div class="modal-footer">
				        	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				        	<button type="submit" class="btn btn-info" data-loading-text="Procesando..." id="btn-enviar-cliente"> Registrar </button>
				      	</div>
			      	</form>
		    	</div>
		  	</div>
		</div>

		<!-- Modal   -->
		<div class="modal fade" id="ventana_gestion_creditos" tabindex="-1" role="dialog">
		  	<div class="modal-dialog modal-sm">
		    	<div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        	<h4 class="modal-title">Gestión de créditos </h4>
			      	</div>
			      	
				    <div class="modal-body">
			        	<div class="row">
			        		<div class="col-xs-12">
			        			<div class="form-group">
								    <label class="label-control" for="monto">Monto</label>
								    <div class="input-group">
								    	<input name="monto" id="monto" type="text" class="form-control">
								    	<span class="input-group-addon"> Créditos </span>
								    </div>
								</div>
			        		</div>
			        		<div class="col-xs-12" id="mensaje_monto"></div>
			        		<div class="col-xs-12 col-sm-6 col-left">
			        			<button class="btn btn-info btn-gestion-credito" onclick="gestion_creditos('aumentar')"> Aumentar  </button>
			        		</div>
			        		<div class="col-xs-12 col-sm-6 col-right">
			        			<button class="btn btn-warning btn-gestion-credito" onclick="gestion_creditos('disminuir')"> Disminuir </button>
			        		</div>
			        		<div class="col-xs-12"> 
			        			<hr>
			        		</div>
			        		<div class="col-xs-12"> 
			        			<h4>
			        				Movimientos
			        			</h4>
			        		</div>
			        		<div class="col-xs-12" id="contenedor_movimientos">
			        			
			        		</div>
			        	</div>
			      	</div>
			      	<div class="modal-footer">
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			      	</div>
		      	
		    	</div>
		  	</div>
		</div>

		<!-- Modal   -->
		<div class="modal fade" id="ventana_gestion_dispositivos" tabindex="-1" role="dialog">
		  	<div class="modal-dialog">
		    	<div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        	<h4 class="modal-title">Gestión de dispositivos </h4>
			      	</div>
			      	
				    <div class="modal-body">
			        	<div class="row">
			        		<div class="col-xs-12">
			        			<div class="form-group">
								    <label class="label-control" for="Codigo">Codigo</label>
								    <input name="Codigo" id="id_dispositivo" type="text" class="form-control">
								</div>
			        		</div>
			        		<div class="col-xs-12" id="mensaje_dispositivo"></div>
			        		<div class="col-xs-12 text-right">
			        			<button class="btn btn-info" id="btn-gestion-dispositivos"> Agregar </button>
			        		</div>
			        		<div class="col-xs-12"> 
			        			<hr>
			        		</div>
			        		<div class="col-xs-12"> 
			        			<h4>
			        				Dispositivos del usuario
			        			</h4>
			        		</div>
			        		<div class="col-xs-12" id="contenedor_dispositivos">
			        			
			        		</div>
			        	</div>
			      	</div>
			      	<div class="modal-footer">
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			      	</div>
		      	
		    	</div>
		  	</div>
		</div>
		
	</body>
</html>

		
	</body>
</html>
