<?php 
	session_start();

	if(!isset($_SESSION["id_usuario"])){
		header("location: index.php");
		die();
	}

	include "../php/conexion.php";

 ?>
<table id="tabla_movimientos_clientes" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th> ID del dispositivo </th>
			<th> Fecha de vigencia </th>
			<th> Acciones </th>
		</tr>
	</thead>
	<tbody>
		<?php 
			
			$sql = "SELECT 
						id_dispositivo, 
						date_format(fecha_vigencia, '%d/%m/%Y') as s_fecha_vigencia,
						datediff( fecha_vigencia, now() ) as diferencia

					FROM 
						t_dispositivos
					WHERE
						id_cliente = '$_POST[id_cliente]' 
					ORDER BY 
						fecha_registro desc";

			$res = mysqli_query($conexion,$sql);

			while( $ln = mysqli_fetch_array($res) ){

				$class = "";

				if($ln['diferencia'] < 7){ //si es 0 es el mismo dia de vigencia
					$class = "warning";					
				}

				if($ln['diferencia'] < 0){ //si es 0 es el mismo dia de vigencia
					$class = "danger";					
				}
				
				echo "<tr
						class='$class'
						data-id='$ln[id_dispositivo]'
					  >
						<td> $ln[id_dispositivo] </td>
						<td> $ln[s_fecha_vigencia] </td>
						<td class='no-wrap'>
							<button type='button' class='btn btn-default editar_dispositivo' title='Editar dispositivo'>
								<span class='glyphicon glyphicon-pencil'></span>
							</button>
							<button type='button' class='btn btn-danger eliminar_dispositivo' title='Eliminar dispositivo'>
								<span class='glyphicon glyphicon-trash'></span>
							</button>
						</td>
					 </tr>";
			}

		 ?>
	</tbody>
</table>