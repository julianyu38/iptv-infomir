<?php 
	session_start();

	if(!isset($_SESSION["id_usuario"])){
		header("location: index.php");
		die();
	}

	if($_SESSION["tipo_usuario"] == "V"){ //solo para administradores y super vendedores
		header("location: index.php");
		die();
	}

	include "../php/conexion.php";

 ?>
<table id="tabla_usuarios" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th> Nombre del Revendedor </th>
			<th> Usuario </th>
			<th> Contraseña </th>
			<th> Tipo de usuario </th>
			<th> Estatus </th>
			<th> Registrado por </th>
			<th> Créditos </th>
			<th> Acciones </th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$where = "";
			if($_SESSION['tipo_usuario'] == "S"){
				//Super vendedor
				$where = "AND tu.id_usuario_registro = '$_SESSION[id_usuario]'";
			}

			$sql = "SELECT 
						tu.id_usuario, 
						tu.nombre, 
						tu.usuario, 
						tu.pass, 
						tu.tipo_usuario, 
						tu.estatus,
						tu2.nombre as usuario_registro,
						sum(monto) as saldo
					FROM 
						t_usuarios as tu 
							inner join 
						t_usuarios as tu2 on (tu.id_usuario_registro = tu2.id_usuario )
							left join
						t_movimientos_usuarios as tmu on (tu.id_usuario = tmu.id_usuario)
					WHERE
						tu.tipo_usuario != 'A' $where
					GROUP BY 
						tu.id_usuario 
					ORDER BY 
						tu.tipo_usuario, tu.fecha_registro";

			$res = mysqli_query($conexion,$sql);

			while( $ln = mysqli_fetch_array($res) ){

				$tipo_usuario = $ln['tipo_usuario'];

				switch ($ln['tipo_usuario']) {
					case 'S':
						$tipo_usuario = 'Super Vendedor';
						break;
					case 'V':
						$tipo_usuario = 'Vendedor';
						break;
				}

				if($ln['estatus'] == "A"){
					$estatus = "Activo";
				}else{
					$estatus = "Inactivo";
				}
				
				if($ln["saldo"] == ""){
					$ln["saldo"] = 0;
				}

				echo "<tr
						data-id='$ln[id_usuario]'
						data-nombre='$ln[nombre]'
						data-usuario='$ln[usuario]'
						data-pass='$ln[pass]'
						data-tipo='$ln[tipo_usuario]'
						data-estatus='$ln[estatus]'
					  >
						<td> $ln[nombre] </td>
						<td> $ln[usuario] </td>
						<td> $ln[pass] </td>
						<td> $tipo_usuario </td>
						<td> $estatus </td>
						
						<td> $ln[usuario_registro] </td>
						<td> $ln[saldo] </td>
						<td class='no-wrap'>
							<button type='button' class='btn btn-default editar_usuario' title='Editar Usuario'>
								<span class='glyphicon glyphicon-pencil'></span>
							</button>
							<button type='button' class='btn btn-success gestion_creditos' title='Gestion de créditos'>
								<span class='glyphicon glyphicon-credit-card'></span>
							</button>
							<button type='button' class='btn btn-danger eliminar_usuario' title='Eliminar Usuario'>
								<span class='glyphicon glyphicon-trash'></span>
							</button>
						</td>
					 </tr>";
			}

		 ?>
	</tbody>
</table>
