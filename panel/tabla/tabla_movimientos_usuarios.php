<?php 
	session_start();

	if(!isset($_SESSION["id_usuario"])){
		header("location: index.php");
		die();
	}

	if($_SESSION["tipo_usuario"] == "V"){ //solo para administradores y super vendedores
		header("location: index.php");
		die();
	}

	include "../php/conexion.php";

 ?>
<table id="tabla_movimientos_usuarios" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th> Fecha de movimiento </th>
			<th> Monto </th>
		</tr>
	</thead>
	<tbody>
		<?php 
			
			$sql = "SELECT 
						monto, 
						date_format(fecha_registro, '%d/%m/%Y %H:%i') as s_fecha_registro
					FROM 
						t_movimientos_usuarios
					WHERE
						id_usuario = '$_POST[id_usuario]' 
					ORDER BY 
						fecha_registro desc";

			$res = mysqli_query($conexion,$sql);

			while( $ln = mysqli_fetch_array($res) ){

				
				echo "<tr
						
					  >
						<td> $ln[s_fecha_registro] </td>
						<td> $ln[monto] </td>
					 </tr>";
			}

		 ?>
	</tbody>
</table>