<?php
session_start();

if (!isset($_SESSION["id_usuario"])) {
    header("location: index.php");
    die();
}

include "../php/conexion.php";
?>
<style type="text/css">
    .table > tbody > tr.warning > td{
        background-color: #FFFD73;
    }
    .table > tbody > tr.danger > td{
        background-color: #FF7673;
    }
</style>
<table id="tabla_clientes" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th> Nombre </th>
            <th> Telefono </th>
            <th> Email </th>           
            <th> Fecha de Expiracion </th>
            <th> ID Dispositivo </th>
            <th> Registrado por </th>
            <th> Creditos </th>
            <th> Dispositivos </th>
            <th> Acciones </th>
        </tr>
    </thead>
    <tbody>
        <?php
        $where = "";

        switch ($_SESSION['tipo_usuario']) {
            case 'V':
                $where = "WHERE tc.id_usuario_registro = '$_SESSION[id_usuario]'";
                break;
            case 'S':
                $where = "WHERE (tc.id_usuario_registro = '$_SESSION[id_usuario]' or tv.id_usuario = '$_SESSION[id_usuario]' )";
                break;
        }


        $sql = "SELECT 
						tc.id_cliente, 
						`nombre_cliente`, 
						`telefono`, 
						`correo`, 						
                                                DATE_FORMAT(td.fecha_vigencia, '%d/%m/%Y') as fecha_vigencia,
						td.id_dispositivo,
                                                tc.id_usuario_registro, 
						tc.fecha_registro,
						tu2.nombre as usuario_registro,
						(
							SELECT 
								sum(monto) 
							FROM 
								t_movimientos_clientes as tmc
							WHERE 
								tc.id_cliente = tmc.id_cliente
						)as saldo,
						count(distinct td.id_dispositivo) as dispositivos,
						sum( case when datediff( fecha_vigencia, now() ) < 0  then 1 else 0  end)  as  cadugados,
						sum( case when datediff( fecha_vigencia, now() ) < 7  then 1 else 0  end)  as  preventivos
					FROM 
						t_clientes as tc 
							left join 
						t_usuarios as tu2 on (tc.id_usuario_registro = tu2.id_usuario )
							left join 
						t_usuarios as tv on (tu2.id_usuario_registro = tv.id_usuario)
							left join
						t_dispositivos td on (tc.id_cliente = td.id_cliente)
					$where
					GROUP BY 
						tc.id_cliente 
					ORDER BY 
						tc.fecha_registro desc";

        //die($sql);

        $res = mysqli_query($conexion, $sql);

        while ($ln = mysqli_fetch_array($res)) {

            if ($ln["saldo"] == "") {
                $ln["saldo"] = 0;
            }
            $opcion_eliminar = "";
/*
            if ($_SESSION["tipo_usuario"] == "A") {
                $opcion_eliminar = "<li role='separator' class='divider'></li>
								    	<li class='eliminar_cliente'><a href='#'>Eliminar Cliente</a></li>";
            }
            */
               $opcion_eliminar = "<li role='separator' class='divider'></li>
								    	<li class='eliminar_cliente'><a href='#'>Eliminar Cliente</a></li>";

            $class = "";

            if ($ln['preventivos'] > 0) { //si es 0 es el mismo dia de vigencia
                $class = "warning";
            }

            if ($ln['cadugados'] > 0) { //si es 0 es el mismo dia de vigencia
                $class = "danger";
            }

            echo "<tr class='$class'
						data-id='$ln[id_cliente]'
						data-nombre='$ln[nombre_cliente]'
						data-telefono='$ln[telefono]'
						data-correo='$ln[correo]'						
                                                data-vigencia='$ln[fecha_vigencia]'
                                                data-id_dispositivo='$ln[id_dispositivo]'>
						<td> $ln[nombre_cliente] </td>
						<td> $ln[telefono] </td>
						<td> $ln[correo] </td>						
                                                <td> $ln[fecha_vigencia] </td> 
                                                <td> $ln[id_dispositivo] </td>
						<td> $ln[usuario_registro] </td>
						<td> $ln[saldo] </td>
						<td> $ln[dispositivos] </td>
						
						<td class='no-wrap'>
							<div class='btn-group'>
							  <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown'>
							    Mas <span class='caret'></span>
							  </button>
							  <ul class='dropdown-menu dropdown-menu-right'>
							    <li class='editar_cliente'><a href='#'>Editar Cliente</a></li>
							    <li role='separator' class='divider'></li>
							    <li class='gestion_creditos'><a href='#'> Administrar Creditos </a></li>
							    <li class='gestion_dispositivos'><a href='#'> Agregar Dispositivos </a></li>
                                                          <li class='gestion_demos'><a href='demos.php'> Demos </a></li> 
							    $opcion_eliminar
							  </ul>
							</div>
						</td>
					 </tr>";
        }
        ?>
    </tbody>
</table>
